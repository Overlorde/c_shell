#ifndef C_SHELL_SHELL_FCT_H
#define C_SHELL_SHELL_FCT_H

#include "cmd.h"
#include <errno.h>
#include <fcntl.h>
#include <fcntl.h>
#include <pwd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
//Terminate shell
#define MY_SHELL_FCT_EXIT 1

//Execute a command
int execCommand(cmd *myCmd);

#endif
