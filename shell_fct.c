#include "shell_fct.h"

#define STDIN 0
#define STDOUT 1
#define STDERR 2

pid_t *childHouse;
unsigned int n;

void alarmHandler(__attribute__((unused)) int sigNum)
{
    for (unsigned int index = 0; index < n; index++)
    {
        kill(childHouse[index], SIGKILL);
    }
}

int execCommand(cmd *myCmd)
{
    if (myCmd->nbCmdMembers < 1)
    {
        printf("Number of commands is not enough.\n");
        return EXIT_FAILURE;
    }
    myCmd->originalStdin = dup(STDIN_FILENO);
    const char *variableExit = "exit";
    const char *chRep = "cd";
    childHouse = (pid_t *)malloc(myCmd->nbCmdMembers * sizeof(pid_t));
    int out;
    int tube[myCmd->nbCmdMembers][2]; // one tube per child
    int status[5];
    char buffer[500];
    // Do the fork each time the child before finishes his task
    if (myCmd->cmdMembersArgs != NULL)
    {
        if (strcmp(myCmd->initCmd, variableExit) == 0)
        {
            free(childHouse);

            return EXIT_FAILURE;
        }
        if (strcmp(myCmd->cmdMembersArgs[0][0], chRep) == 0)
        {
            if (myCmd->cmdMembersArgs[0][1] == NULL)
            {
                if (chdir("/home") == -1)
                {
                    perror("Error, problem with the path");
                    free(childHouse);

                    return 255;
                }
                free(childHouse);

                return EXIT_SUCCESS;
            }
            if (chdir(myCmd->cmdMembersArgs[0][1]) == -1)
            {
                perror("Error, problem with the path");
                free(childHouse);
                return 255;
            }
            free(childHouse);

            return EXIT_SUCCESS; // Allow to try another command
        }
    }

    for (unsigned int n = 0; n < myCmd->nbCmdMembers; n++) // TODO FIX | pipe not working
    {
        if (pipe(tube[n]) == -1) // create child pipe
        {
            perror("pipe has failed ");
            free(childHouse);

            return 255;
        }
        childHouse[n] = fork(); // Outside the CHILD
        if (childHouse[n] == 0)
        {
            // we use read to send the contents of file1 to the buffer
            // redirect stdout to tube 1
            /// Only for first
            if (n == 0)
            {
                close(tube[n][0]); // close the read of pipe to be certain that child is not receiving its own input
                if (myCmd->nbCmdMembers == 1)
                {
                    dup2(tube[n][1], 0); // connect to write side with stdin 0 or 1 stdout -> pipe
                }
                else
                {
                    dup2(tube[n][1], 1); // there is at least one pipe
                }

                close(tube[n][1]); // close write side of pipe
            }
            else
            {
                /// ACTION TO DUPLICATE AFTER FOR EACH CHILD
                dup2(tube[n - 1][0], 0);
                close(tube[n - 1][0]); // COPY THE FORMER CHILD TUBE COMPLETES AND CLOSES THE DESCRIPTOR 0
                // redirect stdout to tube I
                close(tube[n][0]);   // CLOSES THE READER OF HIS PIPE AT THE CHILD IN PROGRESS AS IS ONLY FOR WRITING
                dup2(tube[n][1], 1); // COPY ITS TUBE AS DUP2 -> create bug
                close(tube[n][1]);
            }

            if (myCmd->redirection[n][STDIN] != NULL)
            {
                if ((out = open(myCmd->redirection[n][STDIN], O_RDONLY | O_CLOEXEC, 0777)) == -1)
                {
                    perror("Error occurred when opening the file");
                    free(childHouse);

                    return 255;
                }
                dup2(out, 0);
                close(out);
            }
            else if (myCmd->redirection[n][STDOUT] != NULL) // Content is a read file
            {
                if (myCmd->redirectionType[n][STDOUT] == APPEND) // Create if not exist
                {
                    if ((out = open(myCmd->redirection[n][STDOUT], O_WRONLY | O_APPEND | O_CREAT | O_CLOEXEC, 0777)) ==
                        -1)
                    {
                        perror("Error occurred when opening the file");
                        free(childHouse);

                        return 255;
                    }
                    dup2(out, 1);
                    close(out);
                }
                else if (myCmd->redirectionType[n][STDOUT] == OVERRIDE)
                {
                    if ((out = open(myCmd->redirection[n][STDOUT], O_WRONLY | O_CREAT | O_CLOEXEC, 0777)) == -1)
                    {
                        perror("Error occurred when opening the file");
                        free(childHouse);

                        return 255;
                    }
                    lseek(out, 0, SEEK_SET);
                    dup2(out, 1);
                    close(out);
                }
            }
            else if (myCmd->redirection[n][STDERR] != NULL)
            {
                if (myCmd->redirectionType[n][STDERR] == APPEND)
                {
                    if ((out = open(myCmd->redirection[n][STDERR], O_WRONLY | O_APPEND | O_CREAT | O_CLOEXEC, 0777)) ==
                        -1)
                    {
                        perror("Error occurred when opening the file");
                        free(childHouse);

                        return 255;
                    }
                    dup2(out, 2);
                    close(out);
                }
                else if (myCmd->redirectionType[n][STDERR] == OVERRIDE)
                {
                    if ((out = open(myCmd->redirection[n][STDERR], O_WRONLY | O_CREAT | O_CLOEXEC, 0777)) == -1)
                    {
                        perror("Error occurred when opening the file");
                        free(childHouse);

                        return 255;
                    }
                    lseek(out, 0, SEEK_SET);
                    dup2(out, 2);
                    close(out);
                }
            }
            if (myCmd->cmdMembersArgs != NULL && myCmd->cmdMembersArgs[n] != NULL)
            {
                printf(myCmd->cmdMembersArgs[n][0]);
                if (execvp(myCmd->cmdMembersArgs[n][0], myCmd->cmdMembersArgs[n]) == -1) // ex "ls" "ls -l null"
                {
                    perror("Command executed not recognized !");
                    free(childHouse);

                    exit(errno);
                }
            }
        }
        else if (childHouse[n] == -1)
        {
            perror("Child has encountered and error ");
            free(childHouse);
            exit(errno);
        }
        else
        {
            // Resume from parent
            if (n > 1) // at least one pipe
            {
                close(tube[n - 1][0]); // Means that there is at least two pipes created by another member so the preceding pipe must be closed in read for the parent who will not read it because previous child
            }
            close(tube[n][1]); // The parent does not need to write in the pipe only to read with dup2
        }
    }

    // Wait for all child processes to finish
    for (unsigned int i = 0; i < myCmd->nbCmdMembers; i++) {
        waitpid(childHouse[i], &status[i], 0);
    }
    // Back to parent process
    signal(SIGALRM, alarmHandler);
    // Setting up the timer
    alarm(5); // If after 5s the child does not finish its task then it is killed
    
    // Read and print output from the last child process
    ssize_t bytesRead;
    unsigned int readLoopCounter = myCmd->nbCmdMembers - 1; // Use a separate counter
    while ((bytesRead = read(tube[readLoopCounter][0], buffer, sizeof(buffer))) > 0) {
        write(STDOUT_FILENO, buffer, bytesRead); // Print to stdout
    }

    // Reset stdin redirection
    dup2(myCmd->originalStdin , STDIN_FILENO);

    signal(SIGALRM, SIG_IGN);
    free(childHouse); // Cancel timer to free as children are successful

    return EXIT_SUCCESS;
}
