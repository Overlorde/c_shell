#ifndef C_SHELL_CMD_H
#define C_SHELL_CMD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MY_SHELL_CMD_OK 0
#define APPEND 1
#define OVERRIDE 2

typedef struct {
    //the command originally inputted by the user
    char *initCmd;

    //number of members
    unsigned int nbCmdMembers;

    //each position holds a command member
    char **cmdMembers;

    //cmd_members_args[i][j] holds the jth argument of the ith member
    char ***cmdMembersArgs;

    //number of arguments per member
    unsigned int *nbMembersArgs;

    //the path to the redirection file
    char ***redirection;

    //the redirection type (append vs. override)
    unsigned int **redirectionType;

    //store the original stdin file descriptor
    int originalStdin;
} cmd;

//Prints the command
__attribute__((unused)) void printCmd(const cmd *cmd);

//Frees memory associated to a cmd
void freeCmd(cmd *cmd);

//Initializes the initial_cmd, membres_cmd et nb_membres fields
void parseMembers(const char *inputString, cmd *c);

#endif
