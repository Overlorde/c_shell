#include "shell_fct.h"
#include <pwd.h>
#include <readline/history.h>
#include <readline/readline.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(void)
{
    int ret = MY_SHELL_CMD_OK;
    char *readlineptr = NULL;
    char str[1024];
    char hostname[256];
    char workingdirectory[256];
    cmd command = {0}; // Static allocation for command

    using_history();

    fputs("\n /$$$$$$$$ /$$$$$$ /$$$$$$$$  /$$$$$$  /$$   /$$        /$$$$$$  /$$   /$$ /$$$$$$$$ /$$       /$$\n", stdout);
    fputs("|__  $$__/|_  $$_/|__  $$__/ /$$__  $$| $$$ | $$       /$$__  $$| $$  | $$| $$_____/| $$      | $$\n", stdout);
    fputs("   | $$     | $$     | $$   | $$  \\ $$| $$$$| $$      | $$  \\__/| $$  | $$| $$      | $$      | $$\n", stdout);
    fputs("   | $$     | $$     | $$   | $$$$$$$$| $$ $$ $$      |  $$$$$$ | $$$$$$$$| $$$$$   | $$      | $$\n", stdout);
    fputs("   | $$     | $$     | $$   | $$__  $$| $$  $$$$       \\____  $$| $$__  $$| $$__/   | $$      | $$\n", stdout);
    fputs("   | $$     | $$     | $$   | $$  | $$| $$\\  $$$       /$$  \\ $$| $$  | $$| $$      | $$      | $$\n", stdout);
    fputs("   | $$    /$$$$$$   | $$   | $$  | $$| $$ \\  $$      |  $$$$$$/| $$  | $$| $$$$$$$$| $$$$$$$$| $$$$$$$$\n", stdout);
    fputs("   |__/   |______/   |__/   |__/  |__/|__/  \\__/       \\______/ |__/  |__/|________/|________/|________/\n", stdout);

    printf("\nTIPS: to quit the shell type : exit\n");

    while (ret != MY_SHELL_FCT_EXIT)
    {

        // Get your session info
        gethostname(hostname, sizeof(hostname));
        getcwd(workingdirectory, sizeof(workingdirectory));

        // Print it to the console
        snprintf(str, sizeof(str), "\n{TITAN_SHELL}%s@%s:%s$ ", getpwuid(getuid())->pw_name, hostname, workingdirectory);

        readlineptr = readline(str); // Command from user

        if (readlineptr != NULL) // empty command
        {
            add_history(readlineptr);            // Add history command
            parseMembers(readlineptr, &command); // Parse the command
            // printCmd(command); only for debug (alternate with parsing)

            // Execute the command
            ret = execCommand(&command); // Execute the command
        }
        // Clean the house
        freeCmd(&command);
        free(readlineptr);
    }
    rl_clear_history();
    return EXIT_SUCCESS;
}
