#include "cmd.h"
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#define STDIN 0
#define STDOUT 1
#define STDERR 2

// Prints the command
__attribute__((unused)) void printCmd(const cmd *cmd)
{
    printf("\nDEBUG : Command executed: %s\n\n", cmd->initCmd); // todo create exe for shell with windows
}

// Initializes the initial_cmd, members_cmd et nb_members fields
void parseMembers(const char *inputString, cmd *cmd)
{
    cmd->nbCmdMembers = 1; // default is one command

    // Allocate memory for initCmd and redirection array
    cmd->initCmd = strdup(inputString);
    cmd->redirection = (char ***)malloc(3 * sizeof(char **));

    if (!cmd->initCmd || !cmd->redirection)
    {
        perror("Memory allocation failed. Exiting.");
        exit(EXIT_FAILURE);
    }

    char *next = cmd->initCmd; // Use a pointer to iterate through inputString
    while (*next != '\0')
    {
        if (*next == '|')
        {
            cmd->nbCmdMembers++;
        }
        next++;
    }

    // Allocate memory for cmdMembers, cmdMembersArgs, redirectionType, and nbMembersArgs
    cmd->cmdMembers = (char **)malloc(cmd->nbCmdMembers * sizeof(char *));
    cmd->cmdMembersArgs = (char ***)malloc(cmd->nbCmdMembers * sizeof(char **));
    cmd->redirectionType = (unsigned int **)malloc(cmd->nbCmdMembers * sizeof(unsigned int *));
    cmd->nbMembersArgs = (unsigned int *)calloc(cmd->nbCmdMembers, sizeof(unsigned int));

    if (!cmd->cmdMembers || !cmd->cmdMembersArgs || !cmd->redirectionType || !cmd->nbMembersArgs)
    {
        perror("Memory allocation failed. Exiting.");
        exit(EXIT_FAILURE);
    }

    for (unsigned int i = 0; i < cmd->nbCmdMembers; i++)
    {
        cmd->cmdMembers[i] = NULL;
        cmd->cmdMembersArgs[i] = NULL;

        cmd->redirectionType[i] = (unsigned int *)malloc(3 * sizeof(unsigned int));
        if (!cmd->redirectionType[i])
        {
            perror("Memory allocation failed. Exiting.");
            exit(EXIT_FAILURE);
        }

        memset(cmd->redirectionType[i], STDIN, 3);
    }

    /****************SLICE THE COMMAND (IF PIPE) IN EACH COMMAND STRING************/
    // Initialize cmdMembers using strtok_r
    cmd->cmdMembers = (char **)malloc(cmd->nbCmdMembers * sizeof(char *));
    if (!cmd->cmdMembers)
    {
        perror("Memory allocation failed. Exiting.");
        exit(EXIT_FAILURE);
    }

    next = NULL;
    char *cmdTemp = cmd->initCmd;
    unsigned int index = 0;

    while (index < cmd->nbCmdMembers)
    {
        char *token = strtok_r(cmdTemp, "|", &next);
        if (!token)
        {
            break;
        }
        cmd->cmdMembers[index] = strdup(token);
        if (!cmd->cmdMembers[index])
        {
            perror("Memory allocation failed. Exiting.");
            exit(EXIT_FAILURE);
        }
        cmdTemp = NULL; // Only initialize cmdTemp once

        index++;
    }

    if (index < cmd->nbCmdMembers)
    {
        fprintf(stderr, "Error: Insufficient number of command members.\n");
        exit(EXIT_FAILURE);
    }


    /*************REGISTER CREATED BY THE NUMBER OF COMMAND***********/
    // 3 registers for each command (can be optimized)
    cmd->redirection = (char ***)malloc(cmd->nbCmdMembers * sizeof(char **));

    if (!cmd->redirection)
    {
        printf("Memory allocation has failed. Exit.\n");
        exit(EXIT_FAILURE);
    }

    for (unsigned int i = 0; i < cmd->nbCmdMembers; i++)
    {
        cmd->redirection[i] = (char **)calloc(3, sizeof(char *));
        if (!cmd->redirection[i])
        {
            printf("Memory allocation has failed. Exit.\n");
            exit(EXIT_FAILURE);
        }
    }
    //printf("%s\n",cmd->cmdMembers[1]);

    /****************COUNT NUMBER OF ARGS PER STRING***************/
    // Allocate memory for cmdMembersArgs outside the loop
    cmd->cmdMembersArgs = (char ***)malloc(cmd->nbCmdMembers * sizeof(char **));
    if (!cmd->cmdMembersArgs)
    {
        printf("Memory allocation has failed. Exit.\n");
        exit(EXIT_FAILURE);
    }

    for (unsigned int i = 0; i < cmd->nbCmdMembers; i++)
    {
        cmd->cmdMembersArgs[i] = (char **)malloc(10 * sizeof(char *)); // Store for each command max 10 args (should be enough)
        if (!cmd->cmdMembersArgs[i])
        {
            printf("Memory allocation has failed. Exit.\n");
            exit(EXIT_FAILURE);
        }
        memset(cmd->cmdMembersArgs[i], 0, 10);
    }

    // Index reset
    unsigned int argMemberNumber = 0;
    next = NULL;
    cmdTemp = NULL;
    for (unsigned int i = 0; i < cmd->nbCmdMembers; i++)
    {
        assert(cmd->cmdMembers[i] != NULL);
        cmdTemp = cmd->cmdMembers[i];
        cmd->nbMembersArgs[i] = 0; // problem with open file can be child pipe
        cmd->cmdMembersArgs[i][argMemberNumber] = strtok_r(cmdTemp, "\n\t\r ", &next);
        do
        {
            argMemberNumber++;
            cmd->nbMembersArgs[i] += 1;

        } while ((cmd->cmdMembersArgs[i][argMemberNumber] = strtok_r(NULL, "\n\t\r ", &next)) != NULL);
        argMemberNumber = 0;
    }
    /*for (unsigned int i = 0; i < cmd->nbCmdMembers; i++) {
        for (unsigned int j = 0; j < cmd->nbMembersArgs[i] + 1; j++) //+1 for null arg
        {
            printf("args %u, %u: %s\n", i, j, cmd->cmdMembersArgs[i][j]);
        }
    }*/
    /*************SORTING REDIRECTIONS BASED ON TYPES OF BELT MEMBER***********/
    /*************CREATION REDIRECTION_TYPE REGISTER******************/
    for (unsigned int i = 0; i < cmd->nbCmdMembers; i++)
    {
        for (unsigned int j = 0; j < cmd->nbMembersArgs[i]; j++)
        {
            assert(cmd->cmdMembersArgs[i][j] != NULL);
            if (strcmp(cmd->cmdMembersArgs[i][j], "2>>") ==
                0) // use the previous cmdMembersArgs else has to be made with +1
            {
                assert(cmd->cmdMembersArgs[i][j + 1] != NULL);
                for (unsigned int k = j; k < cmd->nbMembersArgs[i]; k++)
                {
                    cmd->cmdMembersArgs[i][k] = cmd->cmdMembersArgs[i][k + 1]; // crush 2>> from command
                }
                cmd->redirection[i][STDERR] = strdup(cmd->cmdMembersArgs[i][j]); // nbMembersArgs [I] -1 represents the penultimate argument because last argument is null
                cmd->cmdMembersArgs[i][j] = NULL;
                cmd->nbMembersArgs[i] -= 1; // We delete the last NULL because the one before last becomes null
                cmd->redirectionType[i][STDERR] = APPEND;
            }
            else if (strcmp(cmd->cmdMembersArgs[i][j], "2>") == 0)
            {
                assert(cmd->cmdMembersArgs[i][j + 1] != NULL);
                for (unsigned int k = j; k < cmd->nbMembersArgs[i]; k++)
                {
                    cmd->cmdMembersArgs[i][k] = cmd->cmdMembersArgs[i][k + 1]; // crush 2> from command
                }
                cmd->redirection[i][STDERR] = strdup(cmd->cmdMembersArgs[i][j]);
                cmd->cmdMembersArgs[i][j] = NULL;
                cmd->nbMembersArgs[i] -= 1;
                cmd->redirectionType[i][STDERR] = OVERRIDE;
            }
            else if (strcmp(cmd->cmdMembersArgs[i][j], ">>") == 0)
            {
                assert(cmd->cmdMembersArgs[i][j + 1] != NULL);
                for (unsigned int k = j; k < cmd->nbMembersArgs[i]; k++)
                {
                    cmd->cmdMembersArgs[i][k] = cmd->cmdMembersArgs[i][k + 1]; // crush >> from command
                }
                cmd->redirection[i][STDOUT] = strdup(
                    cmd->cmdMembersArgs[i][j]); // command is after >> so at least index 1 and beyond
                cmd->cmdMembersArgs[i][j] = NULL;
                cmd->nbMembersArgs[i] -= 1;
                cmd->redirectionType[i][STDOUT] = APPEND;
            }
            else if (*cmd->cmdMembersArgs[i][j] == '>')
            {
                assert(cmd->cmdMembersArgs[i][j + 1] != NULL);
                for (unsigned int k = j; k < cmd->nbMembersArgs[i]; k++) // TODO could be factorized
                {
                    cmd->cmdMembersArgs[i][k] = cmd->cmdMembersArgs[i][k + 1]; // crush > from command
                }
                cmd->redirection[i][STDOUT] = strdup(
                    cmd->cmdMembersArgs[i][j]); // TODO create multiple stdout >> >> in one command redirection[i][STDOUT][k] -> 4D array
                cmd->cmdMembersArgs[i][j] = NULL;
                cmd->redirectionType[i][STDOUT] = OVERRIDE;
                cmd->nbMembersArgs[i] -= 1;
            }
            else if (*cmd->cmdMembersArgs[i][j] == '<')
            {
                assert(cmd->cmdMembersArgs[i][j + 1] != NULL);
                for (unsigned int k = j; k < cmd->nbMembersArgs[i]; k++)
                {
                    cmd->cmdMembersArgs[i][k] = cmd->cmdMembersArgs[i][k + 1]; // crush < from command
                }
                cmd->redirection[i][STDIN] = strdup(cmd->cmdMembersArgs[i][j]);
                cmd->cmdMembersArgs[i][j] = NULL;
                cmd->nbMembersArgs[i] -= 1;
                // No need to create a stdin append as its default mode
            }
        }
    }
}

// Frees memory associated to a cmd
void freeCmd(cmd *cmd)
{
    if (cmd == NULL)
        return;

    for (unsigned int i = 0; i < cmd->nbCmdMembers; i++)
    {
        if (cmd->redirection[i] != NULL)
        {
            for (unsigned int j = 0; j < cmd->nbMembersArgs[i]; j++)
            {
                if (cmd->redirection[i][j] != NULL)
                {
                    free(cmd->redirection[i][j]);
                    cmd->redirection[i][j] = NULL;
                }
            }
            free(cmd->redirection[i]);
            cmd->redirection[i] = NULL;
        }
    }
    free(cmd->redirection);
    cmd->redirection = NULL;

    for (unsigned int i = 0; i < cmd->nbCmdMembers; i++)
    {
        if (cmd->redirectionType[i] != NULL)
        {
            free(cmd->redirectionType[i]);
            cmd->redirectionType[i] = NULL;
        }
    }
    free(cmd->redirectionType);
    cmd->redirectionType = NULL;

    for (unsigned int i = 0; i < cmd->nbCmdMembers; i++)
    {
        if (cmd->cmdMembersArgs[i] != NULL)
        {
            free(cmd->cmdMembersArgs[i]);
            cmd->cmdMembersArgs[i] = NULL;
        }
    }
    free(cmd->cmdMembersArgs);
    cmd->cmdMembersArgs = NULL;

    if (cmd->cmdMembers != NULL)
    {
        free(cmd->cmdMembers);
        cmd->cmdMembers = NULL;
    }

    if (cmd->initCmd != NULL)
    {
        free(cmd->initCmd);
        cmd->initCmd = NULL;
    }

    if (cmd->nbMembersArgs != NULL)
    {
        free(cmd->nbMembersArgs);
        cmd->nbMembersArgs = NULL;
    }
}
