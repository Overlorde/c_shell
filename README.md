# C Shell

A Unix Shell powered by C language.

## Installation

You can compile the executable with this command: ```gcc cmd.c main.c shell_fct.c -Wall -g -lreadline -o c_shell``` 

A Makefile is available for an easy install: 
**Using Make**
```bash
make
```
```bash
./c_shell
```
## Usage
```
 /$$$$$$$$ /$$$$$$ /$$$$$$$$ /$$$$$$  /$$   /$$        /$$$$$$  /$$   /$$ /$$$$$$$$ /$$       /$$      
|__  $$__/|_  $$_/|__  $$__//$$__  $$| $$$ | $$       /$$__  $$| $$  | $$| $$_____/| $$      | $$      
   | $$     | $$     | $$  | $$  \ $$| $$$$| $$      | $$  \__/| $$  | $$| $$      | $$      | $$      
   | $$     | $$     | $$  | $$$$$$$$| $$ $$ $$      |  $$$$$$ | $$$$$$$$| $$$$$   | $$      | $$      
   | $$     | $$     | $$  | $$__  $$| $$  $$$$       \____  $$| $$__  $$| $$__/   | $$      | $$      
   | $$     | $$     | $$  | $$  | $$| $$\  $$$       /$$  \ $$| $$  | $$| $$      | $$      | $$      
   | $$    /$$$$$$   | $$  | $$  | $$| $$ \  $$      |  $$$$$$/| $$  | $$| $$$$$$$$| $$$$$$$$| $$$$$$$$
   |__/   |______/   |__/  |__/  |__/|__/  \__/       \______/ |__/  |__/|________/|________/|________/
```
Any command can be run with this shell but **only one of each type of redirection between pipe is possible for now**.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
This project is under MIT License.
